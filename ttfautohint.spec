Name:           ttfautohint
Version:        1.8.4
Release:        1
Summary:        A platform for finely hand-hinting
License:        FTL or GPLv2
URL:            http://www.freetype.org/ttfautohint
Source0:        http://download.savannah.gnu.org/releases/freetype/%{name}-%{version}.tar.gz

BuildRequires:  gcc gcc-c++ freetype-devel harfbuzz-devel pkgconfig qt5-qtbase-devel
Provides:       bundled(gnulib) %{name}-gui = %{version}-%{release} %{name}-libs = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Obsoletes:      %{name}-gui < %{version}-%{release} %{name}-libs < %{version}-%{release}

%description
ttfautohint provides a 99% automated hinting process and a platform for finely
hand-hinting the last 1%. It is ideal for web fonts and supports many scripts:
Latin, Greek, Arabic, Devanagari, Hebrew, Khmer, Myanmar, Thai, and many more.

%package        devel
Summary:        Development library for ttfautohint

%description    devel
Development library files for ttfautohint.

%prep
%setup -q

%build
%configure --disable-silent-rules --disable-static
%make_build

%install
%make_install

%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS NEWS README THANKS TODO *.TXT
%doc doc/img doc/ttfautohint.{html,pdf,txt}
%doc doc/*.js
%license COPYING
%{_bindir}/ttfautohint
%{_bindir}/ttfautohintGUI
%{_libdir}/libttfautohint.so.1*
%{_mandir}/man1/ttfautohint.1*
%{_mandir}/man1/ttfautohintGUI.1*

%files          devel
%license COPYING
%{_includedir}/ttfautohint*.h
%{_libdir}/libttfautohint.so
%{_libdir}/pkgconfig/ttfautohint.pc

%changelog
* Thu Jul 21 2022 zhangfan <zhangfan4@kylinos.cn> - 1.8.4-1
- Update to 1.8.4

* Thu Feb 27 2020 lihao <lihao129@huawei.com> - 1.8.2-2
- Package Init
